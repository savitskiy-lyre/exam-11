import Layout from "./components/UI/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import SignIn from "./containers/SignIn";
import SignUp from "./containers/SignUp";
import MainShop from "./containers/MainShop/MainShop";
import Product from "./containers/Product";
import AddProduct from "./containers/AddProduct";
import NotificationToast from "./components/UI/NotificationToast/NotificationToast";

const App = () => {
    return (
        <Layout>
            <NotificationToast/>
            <Switch>
                <Route path="/" exact component={MainShop}/>
                <Route path="/categories" component={MainShop}/>
                <Route path="/products/add" component={AddProduct}/>
                <Route path="/products:id" component={Product}/>
                <Route path="/signin" component={SignIn}/>
                <Route path="/signup" component={SignUp}/>
            </Switch>
        </Layout>
    );
};

export default App;
