import React, {useEffect} from 'react';
import Box from "@mui/material/Box";
import {useDispatch, useSelector} from "react-redux";
import {fetchProducts} from "../../store/actions/productsActions";
import {Link, useLocation} from "react-router-dom";
import {Card, CardActionArea, CardContent, CardMedia, Grid, Stack, Typography} from "@mui/material";
import Button from "@mui/material/Button";
import NotFoundContent from "../../components/UI/NotFoundContent/NotFoundContent";
import {BASE_URL} from "../../config";
import OoopsImg from '../../assets/images/oops.jpg';
import SkeletonRow from "../../components/UI/SkeletonRow/SkeletonRow";

const MainShop = () => {
    const dispatch = useDispatch();
    const location = useLocation();
    const products = useSelector(state => state.products.data);
    const profile = useSelector(state => state.profile.data);
    useEffect(() => {
        dispatch(fetchProducts(location.search))
    }, [dispatch, location.search])

    return (

        <Box mt={2}>
            <>
                <Stack my={3} spacing={5}>
                    {profile && (<Button
                            component={Link}
                            variant={"contained"}
                            to={'products/add'}
                        >
                            Add new product
                        </Button>
                    )}
                    {!products && (
                        <>
                            <SkeletonRow/>
                            <SkeletonRow/>
                            <SkeletonRow/>
                        </>
                    )}
                    {products?.length === 0 && (
                        <NotFoundContent name={'products'}/>
                    )}
                </Stack>
                {products?.length > 0 && (
                    <Grid container spacing={2} justifyContent={"center"} textAlign={"center"}>
                        {products.map(product => {
                            return (
                                <Grid item key={product._id} spacing={2} xs={4}>
                                    <Card>
                                        <CardActionArea
                                            component={Link}
                                            to={'/products' + product._id}
                                        >
                                            <CardContent>
                                                <CardMedia
                                                    component="img"
                                                    height="140"
                                                    image={product.image ? BASE_URL + '/' + product.image : OoopsImg}
                                                    alt="pic"
                                                />
                                                <Typography variant={'h4'}>
                                                    {product.title}
                                                </Typography>
                                                <Typography variant={"subtitle2"}>
                                                    {product.price} Som
                                                </Typography>
                                            </CardContent>
                                        </CardActionArea>
                                    </Card>
                                </Grid>
                            );
                        }).reverse()}
                    </Grid>

                )}
            </>
        </Box>
    );
};

export default MainShop;