import React, {useEffect} from 'react';
import FormRegister from "../components/UI/Form/FormRegister";
import Box from "@mui/material/Box";
import {useDispatch, useSelector} from "react-redux";
import {Alert, Divider} from "@mui/material";
import {signIn, signInFailure} from "../store/actions/profileActions";

const SignIn = () => {
    const errLogIn = useSelector((state) => state.profile.errLogIn);
    const dispatch = useDispatch();

    useEffect(() => () => {
        dispatch(signInFailure(null));
    }, [dispatch]);

    return (
        <Box justifyContent={"center"} width={'100%'} pt={8}>
            <FormRegister
                actionName='Sign in'
                helperLinkName={"Don't have an account? Sign Up"}
                toLocation='/signup'
                onSubmit={(data) => dispatch(signIn(data))}
            />
            {errLogIn && (
                <>
                    <Divider sx={{margin: '20px 0'}}/>
                    <Alert severity="error">{errLogIn.error}</Alert>
                </>
            )}
        </Box>
    );
};

export default SignIn;