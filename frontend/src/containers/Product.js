import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {useParams} from "react-router-dom";
import {deleteProduct, fetchProduct} from "../store/actions/productsActions";
import {CardMedia, Grid, Paper, Stack, Typography} from "@mui/material";
import {BASE_URL} from "../config";
import OoopsImg from "../assets/images/oops.jpg";
import {LoadingButton} from "@mui/lab";

const Product = () => {
    const dispatch = useDispatch();
    const product = useSelector(state => state.products.current)
    const deleteLoading = useSelector(state => state.products.deleteLoading)
    const {id} = useParams();
    useEffect(() => {
        dispatch(fetchProduct(id));
    }, [dispatch, id])
    const profile = useSelector(state => state.profile.data);
    const products = useSelector(state => state.products);
    return (
        <Grid container mt={8} textAlign={"center"} flexDirection={"column"}>
            {product && (
                <>
                    {profile?._id === products.current.author._id && (
                        <LoadingButton loading={deleteLoading}
                                       variant={"contained"}
                                       color={"error"}
                                       sx={{marginBottom: '30px'}}
                                       onClick={()=> dispatch(deleteProduct(id))}
                        >
                            Delete
                        </LoadingButton>
                    )}
                    <Grid item container justifyContent={"center"}>
                        <CardMedia
                            component="img"
                            height="480"
                            sx={{maxWidth: '750px'}}
                            image={product.image ? BASE_URL + '/' + product.image : OoopsImg}
                            alt="pic"
                        />
                    </Grid>
                    <Grid item mt={5}>
                        <Stack spacing={2}>
                            <Typography component={Paper} p={1}>
                                <strong>Title:</strong> {product.title}
                            </Typography>
                            <Typography component={Paper} p={1}>
                                <strong>Description:</strong> {product.description}
                            </Typography>
                            <Typography component={Paper} p={1}>
                                <strong>Price:</strong> {product.price} Som
                            </Typography>
                            <Typography component={Paper} p={1}>
                                <strong>Category:</strong> {product.category.name}
                            </Typography>
                            <Stack component={Paper} spacing={2} p={2}>
                                <Typography variant={"h5"}>
                                    Author: {product.author.display_name}
                                </Typography>
                                <Typography variant={"h5"}>
                                    Phone: {product.author.phone}
                                </Typography>
                            </Stack>
                        </Stack>
                    </Grid>
                </>
            )}
        </Grid>
    );
};

export default Product;