import React, {useEffect} from 'react';
import Box from "@mui/material/Box";
import {Alert, Divider} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import {createAccount, createAccountFailure} from "../store/actions/profileActions";
import FormSignUp from "../components/UI/Form/FormSignUp";

const SignUp = () => {
    const errAccount = useSelector((state) => state.profile.errAccount);
    const dispatch = useDispatch();

    useEffect(() => () => {
        dispatch(createAccountFailure(null))
    }, [dispatch]);

    return (
        <Box justifyContent={"center"} width={'100%'} pt={8}>
            <FormSignUp
                actionName='Sign up'
                helperLinkName='Already have an account? Sign in'
                toLocation='/signin'
                onSubmit={(data) => dispatch(createAccount(data))}
            />
            {errAccount && (
                <>
                    <Divider sx={{margin: '20px 0'}}/>
                    <Alert severity="error">{errAccount.error}</Alert>
                </>
            )}
        </Box>
    );
};

export default SignUp;