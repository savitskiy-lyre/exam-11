import React, {useState} from 'react';
import Box from "@mui/material/Box";
import Avatar from "@mui/material/Avatar";
import ProductionQuantityLimitsIcon from '@mui/icons-material/ProductionQuantityLimits';
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import {Redirect} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {LoadingButton} from "@mui/lab";
import {Alert, Divider} from "@mui/material";
import {addProduct} from "../store/actions/productsActions";

const initState = {
    title: '',
    description: '',
    price: '',
    image: null,
};

const AddProduct = () => {
        const dispatch = useDispatch();
        const profile = useSelector(state => state.profile.data);
        const categories = useSelector(state => state.categories.data);
        const loading = useSelector(state => state.products.submitBtnLoading);
        const addErr = useSelector(state => state.products.addErr);
        const [state, setState] = useState(initState);
        const [currentOption, setCurrentOption] = useState('');
        const handleSubmit = (event) => {
            event.preventDefault();
            const data = new FormData();
            const sendData = {...state, category: currentOption};
            Object.keys(sendData).forEach(key => {
                data.append(key, sendData[key]);
            })
            dispatch(addProduct(data));
        };
        const handleInpChange = (e) => {
            setState(prevState => ({...prevState, [e.target.name]: e.target.value}));
        };


        if (!profile) {
            return <Redirect to={'/signin'}/>
        }

        return (
            <Box
                mt={8}
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
            >
                <Avatar sx={{m: 1, bgcolor: 'primary.main'}}>
                    <ProductionQuantityLimitsIcon/>
                </Avatar>
                <Typography component="h1" variant="h5">
                    Add new product
                </Typography>
                <Box component="form" onSubmit={handleSubmit} noValidate sx={{mt: 1}}>
                    <TextField
                        margin="normal"
                        label="Title"
                        name="title"
                        value={state.title}
                        onChange={handleInpChange}
                        autoFocus
                        required
                    />
                    <TextField
                        margin="normal"
                        name="price"
                        label="Price"
                        value={state.price}
                        onChange={handleInpChange}
                        required
                    />
                    <TextField
                        margin="normal"
                        name="description"
                        label="Description"
                        multiline
                        rows={6}
                        value={state.description}
                        onChange={handleInpChange}
                        required
                    />
                    <TextField
                        type={"file"}
                        margin="normal"
                        name="image"
                        onChange={(e) => {
                            setState(prevState => ({...prevState, image: e.target.files[0]}))
                        }}
                    />
                    <select style={{margin: '10px auto', display: 'block', padding: '8px', fontSize: '20px'}}
                            value={currentOption}
                            onChange={(e) => {
                                setCurrentOption(e.target.value)
                            }}
                    >
                        <option value="">Categories:</option>
                        {categories && (
                            categories.map(category => {
                                return (
                                    <option value={category._id}
                                            key={category._id}
                                    >
                                        {category.name}
                                    </option>)
                            })
                        )}
                    </select>
                    <LoadingButton
                        loading={loading}
                        type="submit"
                        fullWidth
                        variant="contained"
                        sx={{mt: 3, mb: 2}}
                    >
                        Add
                    </LoadingButton>
                </Box>
                {addErr && (
                    <>
                        <Divider sx={{margin: '20px 0'}}/>
                        <Alert severity="error">{addErr.error}</Alert>
                    </>
                )}
            </Box>
        )
    }
;

export default AddProduct;