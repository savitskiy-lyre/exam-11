import {USERS_SESSIONS_URL, USERS_URL} from "../../config";
import {historyBack, historyPush, historyReplace} from "./historyActions";
import axiosApi from "../../axiosApi";
import {successMessageAction} from "./notificationActions";

export const CREATE_ACCOUNT_REQUEST = 'CREATE_ACCOUNT_REQUEST';
export const CREATE_ACCOUNT_SUCCESS = 'CREATE_ACCOUNT_SUCCESS';
export const CREATE_ACCOUNT_FAILURE = 'CREATE_ACCOUNT_FAILURE';
export const SIGN_IN_REQUEST = 'SIGN_IN_REQUEST';
export const SIGN_IN_SUCCESS = 'SIGN_IN_SUCCESS';
export const SIGN_IN_FAILURE = 'SIGN_IN_FAILURE';

export const USER_LOGOUT = 'USER_LOGOUT';

export const createAccountRequest = () => ({type: CREATE_ACCOUNT_REQUEST});
export const createAccountSuccess = (data) => ({type: CREATE_ACCOUNT_SUCCESS, payload: data});
export const createAccountFailure = (error) => ({type: CREATE_ACCOUNT_FAILURE, payload: error});

export const createAccount = (account) => {
    return async (dispatch) => {
        try {
            dispatch(createAccountRequest());
            const {data} = await axiosApi.post(USERS_URL, account);
            dispatch(createAccountSuccess(data));
            dispatch(historyReplace('/'));
            dispatch(successMessageAction('Account created Success'));

        } catch (error) {
            dispatch(createAccountFailure(error?.response?.data || {error: error.message}));
        }
    }
}
export const signInRequest = () => ({type: SIGN_IN_REQUEST});
export const signInSuccess = (data) => ({type: SIGN_IN_SUCCESS, payload: data});
export const signInFailure = (error) => ({type: SIGN_IN_FAILURE, payload: error});

export const signIn = (account) => {
    return async (dispatch) => {
        try {
            dispatch(signInRequest());
            const {data} = await axiosApi.post(USERS_SESSIONS_URL, account);
            dispatch(signInSuccess(data));
            dispatch(historyBack());
            dispatch(successMessageAction('Login Success'));
        } catch (error) {
            dispatch(signInFailure(error?.response?.data || {error: error.message}));
            //dispatch(errorMessageAction(error?.response?.data.error || error.message));
        }
    }
}

export const userLogout = () => {
    return async (dispatch) => {
        try {
            await axiosApi.delete(USERS_SESSIONS_URL);
            dispatch({type: USER_LOGOUT});
            dispatch(historyPush('/'));
            dispatch(successMessageAction('Log out Success'));
        } catch (err) {
            console.log(err);
        }
    };
};
