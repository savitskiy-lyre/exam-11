import axiosApi from "../../axiosApi";
import {CATEGORIES_URL} from "../../config";
import {errorMessageAction} from "./notificationActions";

export const FETCH_CATEGORIES_REQUEST = 'FETCH_CATEGORIES_REQUEST';
export const FETCH_CATEGORIES_SUCCESS = 'FETCH_CATEGORIES_SUCCESS';
export const FETCH_CATEGORIES_FAILURE = 'FETCH_CATEGORIES_FAILURE';

export const CURRENT_CATEGORY = 'CURRENT_CATEGORY';
export const currentCategory = (data) => ({type: CURRENT_CATEGORY, payload: data})


export const fetchCategoriesRequest = () => ({type: FETCH_CATEGORIES_REQUEST});
export const fetchCategoriesSuccess = (data) => ({type: FETCH_CATEGORIES_SUCCESS, payload: data});
export const fetchCategoriesFailure = (error) => ({type: FETCH_CATEGORIES_FAILURE, payload: error});

export const fetchCategories = () => {
    return async (dispatch) => {
        try {
            dispatch(fetchCategoriesRequest());
            const {data} = await axiosApi.get(CATEGORIES_URL);
            dispatch(fetchCategoriesSuccess(data));
        } catch (error) {
            dispatch(errorMessageAction(error?.response?.data || {error: error.message}));
        }
    }
}