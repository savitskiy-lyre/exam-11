import axiosApi from "../../axiosApi";
import {PRODUCTS_URL} from "../../config";
import {historyReplace} from "./historyActions";
import {successMessageAction} from "./notificationActions";

export const FETCH_PRODUCTS_REQUEST = 'FETCH_PRODUCTS_REQUEST';
export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';
export const FETCH_PRODUCTS_FAILURE = 'FETCH_PRODUCTS_FAILURE';

export const fetchProductsRequest = () => ({type: FETCH_PRODUCTS_REQUEST});
export const fetchProductsSuccess = (data) => ({type: FETCH_PRODUCTS_SUCCESS, payload: data});
export const fetchProductsFailure = (error) => ({type: FETCH_PRODUCTS_FAILURE, payload: error});

export const fetchProducts = (search) => {
    return async (dispatch) => {
        try {
            dispatch(fetchProductsRequest());
            const {data} = await axiosApi.get(PRODUCTS_URL + (search ? search : ''));
            dispatch(fetchProductsSuccess(data));
            dispatch(successMessageAction('Products here wohooo'));
        } catch (error) {
            dispatch(fetchProductsFailure(error?.response?.data || {error: error.message}));
        }
    }
}
export const FETCH_PRODUCT_REQUEST = 'FETCH_PRODUCT_REQUEST';
export const FETCH_PRODUCT_SUCCESS = 'FETCH_PRODUCT_SUCCESS';
export const FETCH_PRODUCT_FAILURE = 'FETCH_PRODUCT_FAILURE';

export const fetchProductRequest = () => ({type: FETCH_PRODUCT_REQUEST});
export const fetchProductSuccess = (data) => ({type: FETCH_PRODUCT_SUCCESS, payload: data});
export const fetchProductFailure = (error) => ({type: FETCH_PRODUCT_FAILURE, payload: error});

export const fetchProduct = (id) => {
    return async (dispatch) => {
        try {
            dispatch(fetchProductRequest());
            const {data} = await axiosApi.get(PRODUCTS_URL + '/' + id);
            dispatch(fetchProductSuccess(data));
            dispatch(successMessageAction('Product here wohooo'));
        } catch (error) {
            dispatch(fetchProductFailure(error?.response?.data || {error: error.message}));
        }
    }
}
export const ADD_PRODUCT_REQUEST = 'ADD_PRODUCT_REQUEST';
export const ADD_PRODUCT_SUCCESS = 'ADD_PRODUCT_SUCCESS';
export const ADD_PRODUCT_FAILURE = 'ADD_PRODUCT_FAILURE';

export const addProductRequest = () => ({type: ADD_PRODUCT_REQUEST});
export const addProductSuccess = () => ({type: ADD_PRODUCT_SUCCESS});
export const addProductFailure = (error) => ({type: ADD_PRODUCT_FAILURE, payload: error});

export const addProduct = (newProduct) => {
    return async (dispatch) => {
        try {
            dispatch(addProductRequest());
            await axiosApi.post(PRODUCTS_URL, newProduct);
            dispatch(addProductSuccess());
            dispatch(historyReplace('/'));
            dispatch(successMessageAction('Product added'));
        } catch (error) {
            dispatch(addProductFailure(error?.response?.data || {error: error.message}));
        }
    }
}
export const DELETE_PRODUCT_REQUEST = 'DELETE_PRODUCT_REQUEST';
export const DELETE_PRODUCT_SUCCESS = 'DELETE_PRODUCT_SUCCESS';
export const DELETE_PRODUCT_FAILURE = 'DELETE_PRODUCT_FAILURE';

export const deleteProductRequest = () => ({type: DELETE_PRODUCT_REQUEST});
export const deleteProductSuccess = () => ({type: DELETE_PRODUCT_SUCCESS});
export const deleteProductFailure = (error) => ({type: DELETE_PRODUCT_FAILURE, payload: error});

export const deleteProduct = (id) => {
    return async (dispatch) => {
        try {
            dispatch(deleteProductRequest());
            await axiosApi.delete(PRODUCTS_URL + '/' + id);
            dispatch(deleteProductSuccess());
            dispatch(historyReplace('/'));
            dispatch(successMessageAction('Product deleted'));
        } catch (error) {
            dispatch(deleteProductFailure(error?.response?.data || {error: error.message}));
        }
    }
}