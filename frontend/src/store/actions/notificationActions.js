import { toast } from "react-toastify";

export const ERROR_MESSAGE = 'ERROR_MESSAGE';
export const SUCCESS_MESSAGE = 'SUCCESS_MESSAGE';

export const successMessageAction = (data) => function (dispatch) {
    dispatch({
        type: ERROR_MESSAGE,
        payload: data
    });
    toast.success(data);
};
export const errorMessageAction = (data) => function (dispatch) {
    dispatch({
        type: SUCCESS_MESSAGE,
    });
    toast.warn(data);
};
