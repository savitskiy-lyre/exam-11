import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import thunk from "redux-thunk";
import {profileReducer} from "./reducers/profileReducer";
import {categoriesReducer} from "./reducers/categoriesReducer";
import {productsReducer} from "./reducers/productsReducer";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
    profile: profileReducer,
    categories: categoriesReducer,
    products: productsReducer,
});
const persistedState = loadFromLocalStorage();

const store = createStore(
    rootReducer,
    persistedState,
    composeEnhancers(applyMiddleware(thunk),
    ));

store.subscribe(() => {
    saveToLocalStorage({
        profile: store.getState().profile,
    });
})

export default store;