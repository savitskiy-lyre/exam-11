import {
    CURRENT_CATEGORY,
    FETCH_CATEGORIES_FAILURE,
    FETCH_CATEGORIES_REQUEST,
    FETCH_CATEGORIES_SUCCESS
} from "../actions/categoriesActions";

const initState = {
    data: null,
    current: 'All categories',
    fetchErr: null,
};

export const categoriesReducer = (state = initState, action) => {
    switch (action.type) {
        case CURRENT_CATEGORY:
            return {...state, current: action.payload}
        case FETCH_CATEGORIES_REQUEST:
            return {...state, fetchErr: null}
        case FETCH_CATEGORIES_SUCCESS:
            return {...state, data: action.payload}
        case FETCH_CATEGORIES_FAILURE:
            return {...state, fetchErr: action.payload}
        default:
            return state;
    }
}