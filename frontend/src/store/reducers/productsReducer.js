import {
    ADD_PRODUCT_FAILURE,
    ADD_PRODUCT_REQUEST,
    ADD_PRODUCT_SUCCESS, DELETE_PRODUCT_FAILURE, DELETE_PRODUCT_REQUEST, DELETE_PRODUCT_SUCCESS,
    FETCH_PRODUCT_FAILURE,
    FETCH_PRODUCT_REQUEST,
    FETCH_PRODUCT_SUCCESS,
    FETCH_PRODUCTS_FAILURE,
    FETCH_PRODUCTS_REQUEST,
    FETCH_PRODUCTS_SUCCESS
} from "../actions/productsActions";

const initState = {
    data: null,
    current: null,
    fetchErr: null,
    addErr: null,
    submitBtnLoading: false,
    deleteLoading: false,
};

export const productsReducer = (state = initState, action) => {
    switch (action.type) {
        case FETCH_PRODUCTS_REQUEST:
            return {...state, fetchErr: null}
        case FETCH_PRODUCTS_SUCCESS:
            return {...state, data: action.payload}
        case FETCH_PRODUCTS_FAILURE:
            return {...state, fetchErr: action.payload}
        case FETCH_PRODUCT_REQUEST:
            return {...state, fetchErr: null}
        case FETCH_PRODUCT_SUCCESS:
            return {...state, current: action.payload}
        case FETCH_PRODUCT_FAILURE:
            return {...state, fetchErr: action.payload}
        case ADD_PRODUCT_REQUEST:
            return {...state, addErr: null, submitBtnLoading: true}
        case ADD_PRODUCT_SUCCESS:
            return {...state, submitBtnLoading: false}
        case ADD_PRODUCT_FAILURE:
            return {...state, addErr: action.payload, submitBtnLoading: false}
        case DELETE_PRODUCT_REQUEST:
            return {...state, deleteErr: null, deleteLoading: true}
        case DELETE_PRODUCT_SUCCESS:
            return {...state, deleteLoading: false}
        case DELETE_PRODUCT_FAILURE:
            return {...state, deleteErr: action.payload, deleteLoading: false}
        default:
            return state;
    }
}