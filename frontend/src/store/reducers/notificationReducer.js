const initialState = {
    error: '123'
};

function notificationReducer(state = initialState, action) {
    switch (action.type) {
        case "ERROR_MESSAGE":
            return state;
        case "SUCCESS_MESSAGE":
            return state;
        default:
            return state;
    }
}

export default notificationReducer;
