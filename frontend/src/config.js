export const BASE_URL = 'http://localhost:9000';
export const USERS_URL = '/users';
export const CATEGORIES_URL = '/categories';
export const PRODUCTS_URL = '/products';
export const IMAGES_URL = '/uploads/';
export const USERS_SESSIONS_URL = '/users/sessions';