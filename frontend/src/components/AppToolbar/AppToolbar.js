import React from 'react';
import {AppBar, Button, Divider, Grid, Stack, Toolbar} from "@mui/material";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import ProfileMenu from "../ProfileMenu/ProfileMenu";
import {userLogout} from "../../store/actions/profileActions";
import MenuSelect from "../MenuSelect/MenuSelect";

const AppToolbar = () => {
    const profile = useSelector((state) => state.profile.data);
    const dispatch = useDispatch();

    return (
        <>

            <AppBar position={"fixed"} color={'customBlack'}>
                <Toolbar>
                    <Grid container justifyContent={"space-between"} alignItems={"center"} flexWrap={"nowrap"} py={'10px'}>
                        <Grid item>
                            <Stack flexDirection={'row'}>
                                <Button component={Link}
                                        to='/'
                                        sx={{color: "inherit"}}
                                >
                                    Flea market
                                </Button>
                            </Stack>
                        </Grid>
                        <Grid item container direction={'row-reverse'}>
                        {profile ? (
                            <ProfileMenu
                                handleLogout={() => {
                                    dispatch(userLogout());
                                }}
                                username={profile.username}
                                locationTo={'/something'}
                            />
                        ) : (
                            <Grid display={"inline-flex"}>

                                <Grid item>
                                    <Button component={Link}
                                            to='/signin'
                                            sx={{color: "inherit"}}
                                    >
                                        Sign in
                                    </Button>
                                </Grid>
                                <Grid item>
                                    <Divider orientation="vertical" sx={{backgroundColor: 'gainsboro',}}/>
                                </Grid>
                                <Grid item>
                                    <Button component={Link}
                                            to='/signup'
                                            sx={{color: "inherit"}}
                                    >
                                        Sign up
                                    </Button>
                                </Grid>
                            </Grid>
                        )
                        }
                            <Grid item sx={{minWidth: 120}} mr={3}>
                                <MenuSelect/>
                            </Grid>
                        </Grid>
                    </Grid>
                </Toolbar>
            </AppBar>
            <Toolbar/>
        </>
    );
};

export default AppToolbar;