import React from 'react';
import {ToastContainer} from "react-toastify";

const NotificationToast = () => {

    return (
        <div>
            <ToastContainer autoClose={2000} />
        </div>
    );
};

export default NotificationToast;