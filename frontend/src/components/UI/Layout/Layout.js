import React from 'react';
import {Container, CssBaseline} from "@mui/material";
import './Layout.css';
import AppToolbar from "../../AppToolbar/AppToolbar";

const Layout = ({children}) => {
    return (
        <div className={'content-height'}>
            <CssBaseline/>
            <AppToolbar/>
            <main className={'bg-color content-grow'}>
                    <Container sx={{minHeight: '90vh' ,display:'flex', flexDirection: 'column'}}>
                        {children}
                    </Container>
            </main>
        </div>
    );
};

export default Layout;