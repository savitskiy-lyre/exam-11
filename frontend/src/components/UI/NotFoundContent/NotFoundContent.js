import React from 'react';
import {Box, Icon, Stack, Typography} from "@mui/material";
import NotInterestedTwoToneIcon from '@mui/icons-material/NotInterestedTwoTone';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles({
    wrapper: {
        backgroundColor: '#f9fcef',
        border: '1px solid gainsboro',
        borderRadius: '6px',
        padding:'8px',
        boxShadow: '4px 4px 8px 0px rgba(34, 60, 80, 0.2)',
    },
});

const NotFoundContent = ({name}) => {
    const classes = useStyles();
    return (
        <Box textAlign={"center"} className={classes.wrapper}>
            <Stack spacing={3} >
                <Icon fontSize={"large"} sx={{margin: '0 auto'}}>
                    <NotInterestedTwoToneIcon fontSize={"large"}/>
                </Icon>
                <Typography variant={'h5'}>
                    Ooops looks like {name} is empty !?
                </Typography>
                <Typography variant={'body1'}>
                    Add something to see a few items here.
                </Typography>
            </Stack>
        </Box>
    );
};

export default NotFoundContent;