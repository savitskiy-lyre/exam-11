import React from 'react';
import {Grid, Stack} from "@mui/material";
import {Skeleton} from "@mui/lab";

const SkeletonRow = () => {
    return (
        <Grid container spacing={2} flexWrap={"nowrap"} my={5}>
            <Grid item>
                <Stack spacing={3}>
                    <Skeleton variant="text"/>
                    <Skeleton variant="circular" width={40} height={40}/>
                    <Skeleton variant="rectangular" width={210} height={118}/>
                </Stack>
            </Grid>
            <Grid item>
                <Stack spacing={3}>
                    <Skeleton variant="text"/>
                    <Skeleton variant="circular" width={40} height={40}/>
                    <Skeleton variant="rectangular" width={210} height={118}/>
                </Stack>
            </Grid>
            <Grid item>
                <Stack spacing={3}>
                    <Skeleton variant="text"/>
                    <Skeleton variant="circular" width={40} height={40}/>
                    <Skeleton variant="rectangular" width={210} height={118}/>
                </Stack>
            </Grid>
            <Grid item>
                <Stack spacing={3}>
                    <Skeleton variant="text"/>
                    <Skeleton variant="circular" width={40} height={40}/>
                    <Skeleton variant="rectangular" width={210} height={118}/>
                </Stack>
            </Grid>
            <Grid item>
                <Stack spacing={3}>
                    <Skeleton variant="text"/>
                    <Skeleton variant="circular" width={40} height={40}/>
                    <Skeleton variant="rectangular" width={210} height={118}/>
                </Stack>
            </Grid>
        </Grid>

    );
};

export default SkeletonRow;