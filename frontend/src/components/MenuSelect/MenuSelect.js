import React, {useEffect, useState} from 'react';
import {FormControl, InputLabel, MenuItem, Select} from "@mui/material";
import {currentCategory, fetchCategories} from "../../store/actions/categoriesActions";
import {useDispatch, useSelector} from "react-redux";
import {Link, useLocation} from "react-router-dom";

const MenuSelect = () => {
    const [category, setCategory] = useState('All categories');
    const dispatch = useDispatch();
    const location = useLocation();
    const categories = useSelector((state) => state.categories.data)
    const selectChange = (event) => {
        dispatch(currentCategory(event.target.value));
        setCategory(event.target.value);
    };
    useEffect(() => {
        dispatch(fetchCategories());
    }, [dispatch])

    return location.pathname === '/' && (
        <FormControl fullWidth>
            <InputLabel id={'categoryShop'}>Category</InputLabel>
            <Select
                size={'small'}
                variant={"filled"}
                color={'warning'}
                labelId={'categoryShop'}
                id={'categoryShop'}
                value={category}
                label="Category"
                onChange={selectChange}
            >
                <MenuItem component={Link} to={'/'} value={'All categories'}>All categories</MenuItem>
                {categories && (
                    categories.map(categ => {
                        return <MenuItem component={Link} to={`?category=${categ._id}`} value={categ.name}
                                         key={categ._id}>{categ.name}</MenuItem>
                    })
                )}
            </Select>
        </FormControl>
    );
};

export default MenuSelect;