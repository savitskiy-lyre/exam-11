const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const {nanoid} = require("nanoid");

const SALT_WORK_FACTOR = 10;

const UserSchema = new mongoose.Schema({
   username: {
      type: String,
      required: [true, 'Username or password is incorrect !?'],
      unique: true,
      validate: {
         validator: async value => {
            const user = await User.findOne({username: value});
            if (user) return false;
         },
         message: 'This user is already registered',
      },
   },
   password: {
      type: String,
      required: [true, 'Username or password is incorrect !?'],
   },
   display_name: {
      type: String,
      required: [true, 'Display name required field'],
   },
   phone: {
      type: String,
      validate: {
         validator: function(v) {
            return /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{2}[-\s\.]?[0-9]{2}[-\s\.]?[0-9]{2}$/im.test(v);
         },
         message: props => `${props.value} is not a valid phone number! Valid pattern looks like +(996)707-56-99-48`
      },
      required: [true, 'User phone number required'],
   },
   token: {
      type: String,
      required: [true, 'Ooops token is gone'],
   }
})


UserSchema.pre('save', async function (next) {
   if (!this.isModified('password')) return next();

   const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
   this.password = await bcrypt.hash(this.password, salt);

   next();
});

UserSchema.set('toJSON', {
   transform: (doc, ret) => {
      delete ret.password;
      return ret;
   },
});

UserSchema.methods.generateToken = function () {
   this.token = nanoid();
}

UserSchema.methods.checkPassword = function (password) {
   if (!password) return false;
   return bcrypt.compare(password, this.password);
};

const User = mongoose.model('users', UserSchema);
module.exports = User;