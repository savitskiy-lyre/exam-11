const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const ProductSchema = new mongoose.Schema({
    title: {
        type: String,
        required: [true, 'Title required !?'],
    },
    description: {
        type: String,
        required: [true, 'Description required !?'],
    },
    price: {
        type: Number,
        min: [0, "Can't be negative number"],
        required: [true, 'Price required !?'],
    },
    image: {
        type: String,
    },
    author: {
        type: mongoose.Schema.Types.ObjectId,
        required: [true, 'Author ID required !?'],
        ref: 'users',
    },
    category: {
        type: mongoose.Schema.Types.ObjectId,
        required: [true, 'Pls select category !?'],
        ref: 'categories',
    }


})

ProductSchema.plugin(idValidator);
const Product = mongoose.model('products', ProductSchema);
module.exports = Product;