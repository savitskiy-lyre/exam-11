const User = require('../models/User');

const authUser = async (req, res, next) => {
   const {username, password} = req.body;
   try {
      const user = await User.findOne({username});
      if (!user) return res.status(401).send({error: 'Invalid username or password'});
      const isMatch = await user.checkPassword(password);
      if (!isMatch) return res.status(401).send({error: 'Invalid username or password'});
      req.user = user;
   } catch (err) {
      return res.status(500).send({error: 'Internal Server Error'});
   }
   next();
}

module.exports = authUser;