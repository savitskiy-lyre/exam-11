const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");
const Category = require("./models/Category");
const Product = require("./models/Product");


const run = async () => {
    await mongoose.connect(config.db.testStorageUrl)

    const collections = await mongoose.connection.db.listCollections().toArray();
    for (const collection of collections) {
        await mongoose.connection.db.dropCollection(collection.name);
    }

    const [user1, user2] = await User.create({
        username: 'test1',
        password: 'test',
        display_name: 'Test',
        phone: '+(996)707-56-99-48',
        token: nanoid(),
    }, {
        username: 'admin1',
        password: 'admin',
        display_name: 'Admin',
        phone: '+(996)707-56-99-58',
        token: nanoid(),
    })

    const [category1, category2, category3, category4] = await Category.create({
        name: 'Cars',
    }, {
        name: 'Cats',
    }, {
        name: 'Dogs',
    }, {
        name: 'Other',
    })
    const [product1, product2, product3, product4] = await Product.create({
        title: 'cat',
            description: 'test description',
            price: 228,
            image: '/fixtures/cat1.jpg',
            author: user1,
            category: category2,
    }, {
        title: 'cat2',
        description: 'test description2',
        price: 228,
        image: '/fixtures/cat1.jpg',
        author: user1,
        category: category2,
    }, {
        title: 'dog',
        description: 'test description',
        price: 322,
        image: '/fixtures/dog1.jpg',
        author: user2,
        category: category3,
    }, {
        title: 'dog2',
        description: 'test description2',
        price: 322,
        image: '/fixtures/dog1.jpg',
        author: user2,
        category: category3,
    },{
        title: 'dog3',
        description: 'test description3',
        price: 322,
        image: '/fixtures/dog1.jpg',
        author: user1,
        category: category1,
    },{
        title: 'cat3',
        description: 'test description3',
        price: 322,
        image: '/fixtures/cat1.jpg',
        author: user2,
        category: category4,
    },)

    await mongoose.connection.close();
}
run().catch(console.error)
