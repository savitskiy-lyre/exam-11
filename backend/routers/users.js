const express = require('express');
const router = express.Router();
const authUser = require('../middleware/authUser');
const User = require('../models/User');

router.post('/', async (req, res) => {
    const {username, password,phone,display_name} = req.body;
    const user = new User({username, password,phone,display_name});

    try {
        user.generateToken();
        await user.save();
        res.send(user);

    } catch (err) {
        if (err?.errors) {
            for (let key of Object.keys(err.errors)) {
                if (err.errors[key].path === 'username' || err.errors[key].path === 'password' || err.errors[key].path === 'phone' || err.errors[key].path === 'display_name') {
                    return res.status(400).send({error: err.errors[key].message});
                }
            }
        }
        res.status(500).send({error: 'Internal Server Error'});
    }
});

router.post('/sessions', authUser, async (req, res) => {
    try {
        req.user.generateToken();
        await req.user.save({validateBeforeSave: false});
        res.send(req.user);
    } catch (err) {
        res.status(500).send({error: 'Internal Server Error'});
    }

});

router.delete('/sessions', async (req, res) => {
    const token = req.get('Authorization');
    const success = {message: 'Not really Success'};
    if (!token) return res.send({message: 'No token Success'});
    const user = await User.findOne({token});
    if (!user) return res.send(success);
    try {
        user.generateToken();
        await user.save({validateBeforeSave: false});
        return res.send({message: 'Success'});
    } catch (err) {
        res.status(500).send({error: 'Internal Server Error'});
    }

});

module.exports = router;