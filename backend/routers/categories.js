const express = require('express');
const router = express.Router();
const Category = require('../models/Category');

router.get('/', async (req, res) => {
    try {
        if (req.query.id) {
            const category = await Category.findById(req.query.id);
            return res.send(category);
        }
        const categories = await Category.find();
        res.send(categories);

    } catch (err) {
        res.status(500).send({error: 'Internal Server Error'});
    }

});

module.exports = router;