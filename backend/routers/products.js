const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Product = require('../models/Product');
const Category = require('../models/Category');
const authUserToken = require("../middleware/authUserToken");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
    try {
        if (req.query.category) {
            const category = await Category.findById(req.query.category);
            if (!category) return res.status(404).send({error: 'Not found category'});
            const products = await Product.find({category});
            return res.send(products);
        }
        const products = await Product.find();
        res.send(products)
    } catch (err) {
        res.status(500).send({error: 'Internal Server Error'});
    }

})
router.get('/:id', async (req, res) => {
    try {
        const product = await Product.findById(req.params.id).populate('author').populate('category');
        return res.send(product);
    } catch (err) {
        res.status(500).send({error: 'Internal Server Error'});
    }
})


router.post('/', [authUserToken, upload.single('image')], async (req, res) => {
    const {title, description, price, category: categoryId} = req.body;
    if (!categoryId) return res.status(400).send({error: 'Choose category'});
    const categoryData = await Category.findById(categoryId);
    if (!categoryData) return res.status(404).send({error: 'Category not found'});
    const newProduct = {title, description, price, author: req.user, category: categoryData._id};
    if (req.file) newProduct.image = '/uploads/' + req.file.filename;
    const product = new Product(newProduct);
    try {
        await product.save();
        res.send(product);
    } catch (err) {
        if (err?.errors) {
            for (let key of Object.keys(err.errors)) {
                if (err.errors[key].path === 'title' || err.errors[key].path === 'description' || err.errors[key].path === 'price' || err.errors[key].path === 'author' || err.errors[key].path === 'category') {
                    return res.status(400).send({error: err.errors[key].message});
                }
            }
        }
        res.status(500).send({error: 'Internal Server Error'});
    }

});

router.delete('/:id', authUserToken, async (req, res) => {
    console.log(req.params.id, 'paraaaaaaaaaaam');
    const product = await Product.findById(req.params.id);
    if (!product) return res.status(404).send({error: 'Not Found'});
    if (product.author === req.user._id) return res.status(403).send({error: "You can't delete this product"});
    try {
        await Product.deleteOne({_id: product._id});
        res.send({message: 'Deletion done'});
    } catch (err) {
        res.status(500).send({error: 'Internal Server Error'});
    }
})

module.exports = router;